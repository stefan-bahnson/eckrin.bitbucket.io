var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var path = require('path');

module.exports = {
  entry: './projects/acumis-minds/index.js',
  output: {
    path: path.resolve(__dirname, 'acumis-minds'),
    filename: 'bundle.js',
    publicPath: '',
  },
  devServer: {
    inline: true,
    historyApiFallback: true,
    port: 3000,
    contentBase: path.join(__dirname, 'acumis-minds'),
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        loader: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [ 'css-loader', 'sass-loader' ]
        })
      },
      {
        test: /\.(jpg|jpeg|png|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'assets/images/[name].[ext]'
        }
      },
      {
        test: /\.(woff|woff2|otf|ttf)$/,
        loader: 'file-loader',
        options: {
          name: 'assets/fonts/[name].[ext]'
        }
      },
    ]
  },
  target: 'web',
  plugins: [
    new ExtractTextPlugin('style.css'),
    new CopyWebpackPlugin([ {
      from: 'projects/acumis-minds/index.html',
      to: 'index.html'
    } ])
  ]
};
